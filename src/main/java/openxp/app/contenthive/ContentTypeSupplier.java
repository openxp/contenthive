package openxp.app.contenthive;


import com.enonic.xp.app.ApplicationKey;
import com.enonic.xp.form.*;
import com.enonic.xp.schema.content.ContentType;
import com.enonic.xp.schema.content.ContentTypeName;
import com.enonic.xp.schema.content.ContentTypeService;
import com.enonic.xp.schema.content.ContentTypes;
import com.enonic.xp.schema.mixin.Mixin;
import com.enonic.xp.schema.mixin.MixinService;
import com.enonic.xp.script.bean.BeanContext;
import com.enonic.xp.script.bean.ScriptBean;
import com.google.common.base.Strings;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.function.Supplier;

@Component(immediate = true)
public class ContentTypeSupplier implements ScriptBean {
    private Supplier<ContentTypeService> contentTypeService;
    private Supplier<MixinService> mixinService;
    Logger LOG = LoggerFactory.getLogger(getClass());

    public LinkedHashSet<ContentTypeSimple.SimpleInput> getContenttypeFormInputs(String appContentType) throws Exception {
        ContentTypeSimple contentTypeSimple = new ContentTypeSimple();

        if (Strings.isNullOrEmpty(appContentType)) {
            throw new Exception("contentType cannot be null or empty");
        }

        if (!appContentType.contains(":")) {
            throw new Exception("contentType is not on correct format applicationKey:contenttype");
        }

        String[] appContentTypeParts = appContentType.split(":");
        String guessedApplicationKey = appContentTypeParts[0];
        String guessedContenType = appContentTypeParts[1];

        ApplicationKey applicationKey = ApplicationKey.from(guessedApplicationKey);
        ContentTypes contentTypes = contentTypeService.get().getByApplication(applicationKey);

        for (ContentType contentType : contentTypes) {
            ContentTypeName contentTypeName = contentType.getName();
            String ctn = contentTypeName.getLocalName();
            if (!ctn.equals(guessedContenType)) {
                continue;
            }
            handleForm(contentType.getForm(), contentTypeSimple);
        }
        return contentTypeSimple.getInputs();
    }

    private void handleForm(Form form, ContentTypeSimple contentTypeSimple) {
        FormItems formItems = form.getFormItems();
        for (FormItem formItem : formItems) {
            handleFormItemType(formItem, contentTypeSimple);
        }
    }

    private void handleFormItemType(FormItem formItem, ContentTypeSimple contentTypeSimple) {
        FormItemType formItemType = formItem.getType();
        if (FormItemType.LAYOUT == formItemType) {
            handleLayout((FieldSet) formItem, contentTypeSimple);
        } else if (FormItemType.FORM_ITEM_SET == formItemType) {
            handleFormItemSet((FormItemSet) formItem, contentTypeSimple);
        } else if (FormItemType.INPUT == formItemType) {
            contentTypeSimple.add((Input)formItem);
        } else if (FormItemType.MIXIN_REFERENCE == formItemType) {
            InlineMixin inlineMixin = (InlineMixin) formItem;
            Mixin mixin = mixinService.get().getByName(inlineMixin.getMixinName());
            handleForm(mixin.getForm(), contentTypeSimple);
        }
    }

    private void handleLayout(FieldSet fieldSet, ContentTypeSimple contentTypeSimple) {
        FormItems formItems = fieldSet.getFormItems();
        for (FormItem formItem : formItems) {
            handleFormItemType(formItem, contentTypeSimple);
        }
    }

    private void handleFormItemSet(FormItemSet formItemSet, ContentTypeSimple contentTypeSimple) {
        FormItems formItems = formItemSet.getFormItems();
        for (FormItem formItem : formItems) {
            handleFormItemType(formItem, contentTypeSimple);
        }
    }

    @Override
    public void initialize(final BeanContext context) {
            this.contentTypeService = context.getService(ContentTypeService.class);
        this.mixinService = context.getService(MixinService.class);
    }
}

package openxp.app.contenthive;


import com.enonic.xp.script.bean.BeanContext;
import com.enonic.xp.script.bean.ScriptBean;
import com.google.common.base.Strings;
import com.google.common.io.ByteSource;
import org.apache.commons.io.IOUtils;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.io.StringWriter;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.net.URLConnection;


@Component(immediate = true)
public class HttpService implements ScriptBean {

    Logger LOG = LoggerFactory.getLogger(this.getClass());

    public ByteSource getImageFromUrl(String urlString, String proxy) throws Exception {
        ByteSource bs = null;
        try {
            InputStream is = getInputStream(urlString, proxy);
            org.apache.shiro.util.ByteSource tmp = org.apache.shiro.util.ByteSource.Util.bytes(is);
            bs = ByteSource.wrap(tmp.getBytes());
        } catch (Exception e) {
            LOG.error("Exception in HttpService {}", e);
        }
        return bs;
    }

    public String getUrlResponse(String urlString, String proxy) throws Exception {
        String response = "";
        try {
            InputStream is = getInputStream(urlString, proxy);
            StringWriter writer = new StringWriter();
            IOUtils.copy(is, writer, "UTF-8");
            response = writer.toString();
        } catch (Exception e) {
            LOG.error("Exception {}", e);
        }
        return response;
    }

    private InputStream getInputStream(String urlString, String proxyServer) throws Exception {
        if (Strings.isNullOrEmpty(proxyServer) || proxyServer.trim().length() < 1) {
            URLConnection urlConnection = new URL(urlString).openConnection();
            return urlConnection.getInputStream();
        } else {
            int split = proxyServer.lastIndexOf(":");
            String proxyHost = proxyServer.substring(0, split);
            Integer proxyPort = Integer.parseInt(proxyServer.substring(split + 1, proxyServer.length()));
            Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(proxyHost.trim(), proxyPort));
            URLConnection urlConnection = new URL(urlString).openConnection(proxy);
            return urlConnection.getInputStream();
        }
    }

    @Override
    public void initialize(BeanContext context) {

    }
}

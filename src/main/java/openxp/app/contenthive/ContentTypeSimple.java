package openxp.app.contenthive;


import com.enonic.xp.form.Input;
import com.enonic.xp.form.Occurrences;

import java.util.LinkedHashSet;

public class ContentTypeSimple {

    public class SimpleInput {
        SimpleInput(Input input){
            this.name = input.getName();
            this.type = input.getInputType().toString().toLowerCase();
            this.path = input.getPath().toString();
            this.elementCount = input.getPath().elementCount();
            Occurrences occurrences = input.getOccurrences();
            this.min = occurrences.getMinimum();
            this.max = occurrences.getMaximum();
            this.isMultiple = occurrences.isMultiple();
            this.impliesRequired = occurrences.impliesRequired();
            this.isUnlimited = occurrences.isUnlimited();
            this.label = input.getLabel();
        }

        String label;
        String name;
        String type;
        String path;
        int elementCount;
        int min;
        int max;
        boolean isMultiple;
        boolean impliesRequired;
        boolean isUnlimited;
        String value;

        public String getName() {
            return name;
        }

        public String getLabel() {
            return label;
        }

        public String getType() {
            return type;
        }

        public String getPath() {
            return path;
        }

        public int getElementCount() {
            return elementCount;
        }

        public int getMin() {
            return min;
        }

        public int getMax() {
            return max;
        }

        public boolean isMultiple() {
            return isMultiple;
        }

        public boolean isImpliesRequired() {
            return impliesRequired;
        }

        public boolean isUnlimited() {
            return isUnlimited;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    private LinkedHashSet<SimpleInput> inputs = new LinkedHashSet<>();


    void add(Input input){
        inputs.add(new SimpleInput(input));
    }

    LinkedHashSet<SimpleInput> getInputs() {
        return inputs;
    }

}

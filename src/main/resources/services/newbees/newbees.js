var libs = {
    content: require('/lib/xp/content'),
    portal: require('/lib/xp/portal'),
    util: require('/lib/util'),
    contentTypeSupplier: __.newBean("openxp.app.contenthive.ContentTypeSupplier"),
    httpService: __.newBean("openxp.app.contenthive.HttpService"),
    cache: require("/lib/cache")
};

var defaultConfig = {
    paragraphs: 'short',
    max: 3
};

var loripsumUrl = ' http://loripsum.net/api';

var pixelCache = libs.cache.newCache({
        size: 100,
        expire: 86400//24 hours cache policy
    }
);

var offlineIpsumForTextLineAndTags = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Et quidem, inquit, vehementer errat; Negat enim summo bono afferre incrementum diem. Aliter enim nosmet ipsos nosse non possumus. Illa videamus, quae a te de amicitia dicta sunt. Ex quo, id quod omnes expetunt, beate vivendi ratio inveniri et comparari potest. Duo Reges: constructio interrete. Quo plebiscito decreta a senatu est consuli quaestio Cn. Haec para doca illi, nos admirabilia dicamus. In eo enim positum est id, quod dicimus esse expetendum. Nam Pyrrho, Aristo, Erillus iam diu abiecti. Zenonis est, inquam, hoc Stoici. Ut id aliis narrare gestiant? Sed mehercule pergrata mihi oratio tua. Quae quo sunt excelsiores, eo dant clariora indicia naturae. Quid iudicant sensus? Qua tu etiam inprudens utebare non numquam. Nonne videmus quanta perturbatio rerum omnium consequatur, quanta confusio? Ea possunt paria non esse. Hoc tu nunc in illo probas. Si alia sentit, inquam, alia loquitur, numquam intellegam quid sentiat; Sed ad haec, nisi molestum est, habeo quae velim. Num igitur eum postea censes anxio animo aut sollicito fuisse? Quae cum essent dicta, discessimus. His singulis copiose responderi solet, sed quae perspicua sunt longa esse non debent. Itaque contra est, ac dicitis; Sed quid sentiat, non videtis. Nunc haec primum fortasse audientis servire debemus. Sed quia studebat laudi et dignitati, multum in virtute processerat. Progredientibus autem aetatibus sensim tardeve potius quasi nosmet ipsos cognoscimus. Sextilio Rufo, cum is rem ad amicos ita deferret, se esse heredem Q. Sint ista Graecorum; Ita graviter et severe voluptatem secrevit a bono. Qui convenit? Praeclare hoc quidem. Quae cum magnifice primo dici viderentur, considerata minus probabantur. Atque haec coniunctio confusioque virtutum tamen a";

function handleReq(req) {
    var params = req.params;
    var id = params.id;
    var number = params.number;
    var parentPath = params.parentPath;
    var data = {};

    var content = libs.content.get({
        key: id
    });

    var siteConfig = libs.content.getSiteConfig({key: content._id, applicationKey:app.name});

    var formInputs = [];

    var pixelapi = params.pixelapi;

    if (pixelapi) {
        return createSomePixels({params: params, siteConfig: siteConfig});

    } else {
        formInputs = libs.contentTypeSupplier.getContenttypeFormInputs(content.type);
    }

    for (var n = 0; n < number; n++) {//number of content to create
        var textContentFromTextAreas = '';
        var imageTags = [];

        formInputs.forEach(function (currentInput) {//for each input in contenttype
            if (!shouldIncludeInputType({
                    includeTypes: ['htmlarea', 'textarea', 'geopoint'],
                    input: currentInput,
                    params: params
                })) {
                return;//stop processing this iteration
            }
            if (currentInput.getType() === 'htmlarea' || currentInput.getType() === 'textarea'){
                var content = createContentNeedingLoremIpsumContent({
                    currentInput: currentInput,
                    data: data,
                    params: params,
                    proxyServer: (siteConfig ? siteConfig.proxyServer : '')
                });
            }else if (currentInput.getType()==='geopoint'){
                var path = currentInput.getPath();
                var randomPoint = generateRandomPoint({
                    lat:params['custom-' + path + '-lat'],lng:params['custom-' + path + '-long']},
                    params['custom-' + path + '-radius']);
                content = randomPoint.lat + "," + randomPoint.lng;
            }
            setDataValue(data, currentInput.getPath(), content);

            if (currentInput.getType() === 'textarea') {
                textContentFromTextAreas += content;
            }
        });

        formInputs.forEach(function (currentInput) {//for each input in contenttype
            if (!shouldIncludeInputType({
                    includeTypes: ['imageselector', 'contentselector'],
                    input: currentInput,
                    params: params
                })) {
                return;//stop processing this iteration
            }
            var currentPath = currentInput.getPath();
            var relatedContent = createRelatedContent({currentInput: currentInput, data: data, params: params});
            imageTags = relatedContent.imageTags;
            setDataValue(data, currentPath, relatedContent.relatedContentIds);
        });

        formInputs.forEach(function (currentInput) {//for each input in contenttype
            if (!shouldIncludeInputType({includeTypes: ['textline', 'tag'], input: currentInput, params: params})) {
                return;//stop processing this iteration
            }
            var currentPath = currentInput.getPath();
            var currentType = currentInput.getType();

            if (textContentFromTextAreas === '') {
                //TODO: Implement possibility for tags, textline etc. without having a TextArea
                textContentFromTextAreas = offlineIpsumForTextLineAndTags;
            }
            var content = '';
            if (currentType === 'textline') {
                content = createTextLineContent({
                    path: currentPath,
                    params: params,
                    textContent: textContentFromTextAreas
                });
            } else if (currentType === 'tag') {
                content = createTagContent({path: currentPath, params: params, textContent: textContentFromTextAreas, imageTags:imageTags});
            }

            if (content === '') {
                return;
            }

            setDataValue(data, currentPath, content);

        });

        formInputs.forEach(function (currentInput) {//for each input in contenttype
            if (!shouldIncludeInputType({
                    includeTypes: ['checkbox'],
                    input: currentInput,
                    params: params
                })) {
                return;//stop processing this iteration
            }
            var currentPath = currentInput.getPath();
            var checkedStateParam =  params['custom-' + currentPath + '-checkedstate'];
            var checkedState = true;

            if (checkedStateParam  === undefined){
                return;
            }
            else if (checkedStateParam === 'random'){
                checkedState = getRandomInt(0, 1) !== 0;
            }else if (checkedStateParam === 'false'){
                checkedState = false;
            }else if (checkedStateParam === 'true'){
                checkedState = true;
            }

            setDataValue(data, currentPath, checkedState);
        });

        if (Object.keys(data).length === 0) {
            return
        }

        var displayName = getDisplayNameFromTextContent(textContentFromTextAreas);
        create({content: content, parentPath: parentPath, displayName: displayName, data: data});
        libs.util.log('Created content ' + (n + 1) + ' from ContentHive');
    }

    return {
        contentType: 'application/json',
        body: "success"
    };
}
//https://pixabay.com/api/docs/
function createSomePixels(args) {

    var params = args.params;
    var siteConfig = args.siteConfig;

    var number = params.number;
    var parentPath = params.parentPath;
    var q = params["pixelapi-q"];
    var lang = params["pixelapi-lang"];
    var response_group = params["pixelapi-response_group"];
    var category = params["pixelapi-category"];
    var orientation = params["pixelapi-orientation"];
    var image_type = params["pixelapi-image_type"];
    var min_height = params["pixelapi-min_height"];
    var min_width = params["pixelapi-min_width"];
    var editors_choice = params["pixelapi-editors_choice"] !== undefined;
    var safesearch = params["pixelapi-safesearch"] !== undefined;
    var imageUrl = params["pixelapi-imageUrl"];

    var per_page = 200;
    var fullPages = 1;
    var lastPage;

    //Pixabay accepts per_page of 3-200
    if (parseInt(number, 10) <= 200) {//all images can be fetched in one go
        per_page = number;
        lastPage = number;
        fullPages = 0;
    } else if (parseInt(number, 10) > 200) {
        per_page = 200;
        fullPages = parseInt(number / 200, 10);
        lastPage = parseInt(number % 200, 10);
    }



    var pixelApiUrl = ' https://pixabay.com/api/'
        + '?key=' + (siteConfig ? siteConfig.pixabayApikey : '')
        + '&q=' + q
        + '&lang=' + lang
        + '&category=' + category
        + '&response_group=' + response_group
        + '&orientation=' + orientation
        + '&image_type=' + image_type
        + '&min_height=' + min_height
        + '&min_width=' + min_width
        + '&editors_choice=' + editors_choice
        + '&safesearch=' + safesearch
        + '&per_page=' + per_page;


    var cacheKeyPrefix = (per_page + q + lang + category + orientation + image_type + min_height + min_width + editors_choice + safesearch + response_group).trim();

    var jsonResponse = [];
    try {
        for (var page = 0; page <= fullPages; page++) {

            var cacheKey = cacheKeyPrefix + page;
            var response = pixelCache.get(cacheKey, function () {
                var url = pixelApiUrl + "&page=" + (parseInt(page + 1, 10));
                return getUrlResponse(url, (siteConfig ? siteConfig.proxyServer : ''));
            });

            if (!response) {
                return;
            }

            var hits = JSON.parse(response).hits;
            hits = shuffle(hits);

            if (page === fullPages && lastPage > 0) {//last page
                hits = hits.slice(0, lastPage);
            }
            jsonResponse = jsonResponse.concat(hits);
        }
    } catch (e) {
        libs.util.log("Exception occured when getting images from API.");
        libs.util.log(e);
    }

    jsonResponse.forEach(function (hit) {
        //var url = hit[imageUrl].replace('https', 'http');
        var url = hit[imageUrl];
        var fileName = hit.user + "-" + hit.previewURL.substring(hit.previewURL.lastIndexOf("/") + 1, hit.previewURL.length);
        try {
            var result;
            var data = getImageFromUrl(url, (siteConfig ? siteConfig.proxyServer : ''));
            if (data) {
                result = libs.content.createMedia({
                    name: fileName,
                    parentPath: parentPath,
                    data: data
                });
            } else {
                libs.util.log("Could not get image from " + url);
            }
            libs.content.modify(
                {
                    key: result._id,
                    editor: function (c) {
                        c.data.copyright = "Creative Commons CC0";
                        if (hit.tags){
                            c.data.tags = replaceAll(hit.tags,', ',',').split(',');
                        }
                        if (hit.user && hit.user_id) {
                            c.data.artist = hit.user+ ' (pixabay user_id: ' + hit.user_id + ')';
                        }
                        if (hit.pageURL) {
                            c.data.caption = hit.pageURL;
                        }
                        return c;
                    }
                }
            );
        } catch (e) {
            log.error(e);
        }
    });

    return {
        contentType: 'application/json',
        body: "success"
    };
}

function replaceAll(str, find, replace) {
    return str.replace(new RegExp(find, 'g'), replace);
}

function getDisplayNameFromTextContent(textContent) {
    if (textContent === '') {
        textContent = offlineIpsumForTextLineAndTags;
    }
    var length = parseInt(textContent.length) - 42;
    var randomStartPos = getRandomInt(0, length);
    var randomLength = getRandomInt(5, 42);
    return textContent.substring(randomStartPos, randomStartPos + randomLength);
}

/**
 * @includeTypes []
 * @input
 * @params
 * */
function shouldIncludeInputType(args) {
    var input = args.input;
    var path = input.getPath();
    var type = input.getType();
    var includeTypes = args.includeTypes;

    var includeType = includeTypes.indexOf(type) !== -1;
    var includePath = args.params['custom-' + path + '-include'];

    return (includeType && includePath);
}

function createContentNeedingLoremIpsumContent(args) {
    var currentInput = args.currentInput;
    var params = args.params;

    var type = currentInput.getType();
    var path = currentInput.getPath();

    var randomConfigPool = getRandomConfigPool(params, path);
    var commonConfig = getCommonConfigForInputType(params, path, type);
    var url = generateRandomCommonConfigUrl(commonConfig);

    if (type === 'textarea' || type === 'tag') {
        url = addToUrl(url, 'plaintext');
    }

    url = addNSettingsFromRandomConfigPool(url, randomConfigPool);
    return getUrlResponse(url, args.proxyServer);
}

function createRelatedContent(args) {
    var currentInput = args.currentInput;
    var params = args.params;
    var path = currentInput.getPath();
    var imageTags = [];

    var useOnly = params['custom-' + path + '-useonly'];
    if (useOnly === 'on') {
        var content = libs.content.get({
            key: params.id
        });
        //Use only the currenly selected relatedcontent id for all created content
        var currentContent = content.data[path];
        if (path.contains(".")) {
            var pathParts = path.split(".");
            var firstPath = pathParts[0];
            var secondPath = pathParts[1];
            currentContent = content.data[firstPath][secondPath];
            if (content.data && content.data.tags){
                imageTags = content.data.tags;
            }
        }
        return {relatedContentIds:[currentContent],imageTags:imageTags};
    }


    var min = parseInt(params['custom-' + path + '-min']);
    var max = parseInt(params['custom-' + path + '-max']);
    var contentPath = params['custom-' + path + '-path'];

    var query = "_path LIKE '/content" + contentPath + "/*' ";
    var count = getRandomInt(min, max);

    //Generate a random start index based on count and totalcount
    var getTotalCountResult = libs.content.query({query: query, start: 0, count: 0});

    var totalCount = getTotalCountResult.total;
    var start = getRandomInt(0, totalCount - count);
    var result = libs.content.query({
        start: start,
        count: count,
        query: query
    });

    var relatedContentIds = [];
    result.hits.forEach(function (element) {
        relatedContentIds.push(element._id);
        if (element.data.tags){
            imageTags  = imageTags.concat(element.data.tags);
        }
    });

    return {relatedContentIds:relatedContentIds,imageTags:imageTags};
}


function setDataValue(data, path, content) {
    if (path.contains(".")) {
        var pathParts = path.split(".");
        var firstPath = pathParts[0];
        var secondPath = pathParts[1];
        if (!data[firstPath]) {
            data[firstPath] = {};
        }
        data[firstPath][secondPath] = content;

    } else {
        data[path] = content;
    }
}

function createTagContent(args) {
    var params = args.params;
    var path = args.path;
    var textContent = args.textContent.replace(/[^a-zA-Z ]/g, "");
    var imageTags = args.imageTags;
    var tagsArray = textContent.split(" ");
    var type = params['custom-' + path + '-type'];
    var max = parseInt(params['custom-' + path + '-max']);

    var randomNumberOfTags = getRandomInt(1, max);

    var tags = [];
    for (var i = 0; i < randomNumberOfTags; i++) {
        if (imageTags && imageTags[i]){
            tags.push(imageTags[i]);
        }else{
            var randomIndex = getRandomInt(1, tagsArray.length);
            tags.push(tagsArray[randomIndex - 1]);
        }
    }

    return tags;
}

function createTextLineContent(args) {
    return args.textContent.substring(0, getRandomInt(10, 100));
}

function getCommonConfigForInputType(params, path, type) {
    if (type === 'textarea' || type === 'htmlarea') {
        return {
            paragraphs: params['custom-' + path + '-paragraphs'],
            max: parseInt(params['custom-' + path + '-max'])
        };
    }
    return {};

}

function getUrlResponse(url, proxyServer) {
    return libs.httpService.getUrlResponse(url, proxyServer || '');
}

function fetchUrl(url, proxyServer) {
    return libs.httpService.fetchUrl(url, proxyServer || '');
}

function getImageFromUrl(url, proxyServer) {
    return libs.httpService.getImageFromUrl(url, proxyServer || '');
}


function getRandomConfigPool(params, path) {
    var randomConfigPool = {};
    var paramKeys = Object.keys(params);
    paramKeys.forEach(function (element) {
        var prefix = 'random-' + path + '-';
        if (element.indexOf(prefix) !== -1) {
            randomConfigPool[element.split('-').pop()] = params[element];
        }
    });
    return randomConfigPool;
}

/**
 * @content
 * @displayName
 * @parentPath
 * @data
 * */
function create(args) {
    try {
        libs.content.create({
            parentPath: args.parentPath,
            displayName: args.displayName,
            draft: true,
            contentType: args.content.type,
            language: args.content.language,
            data: args.data,
            requireValid: false

        });
    } catch (e) {
        libs.util.log(e);
    }

}

function addNSettingsFromRandomConfigPool(url, randomConfigPool) {
    var keys = Object.keys(randomConfigPool);
    if (!randomConfigPool || keys.length < 1) {
        return url;
    }

    var randomNumberOfSettingsToAdd = getRandomInt(1, keys.length);
    var configArray = createAndShuffleArrayFromJsonConfig(randomConfigPool);

    for (var i = 0; i < randomNumberOfSettingsToAdd; i++) {
        url = addToUrl(url, configArray.pop());
    }

    return url;
}

function createAndShuffleArrayFromJsonConfig(config) {

    var keys = Object.keys(config);

    var configArray = [];
    for (var x in keys) {
        if (keys.hasOwnProperty(x)) {
            configArray.push(keys[x]);
        }
    }
    return shuffle(configArray);
}

//Fisher-Yates (aka Knuth) Shuffle algorithm
function shuffle(array) {
    var currentIndex = array.length, temporaryValue, randomIndex;

    // While there remain elements to shuffle...
    while (0 !== currentIndex) {

        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;

        // And swap it with the current element.
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }

    return array;
}

function generateRandomCommonConfigUrl(config) {
    //Create random number of paragraphs between 1 and max
    var url = loripsumUrl;
    var max = parseInt(config.max);
    if (!max) max = defaultConfig.max;
    url = addToUrl(url, getRandomInt(1, max));
    url = addToUrl(url, "prude");

    //Average lenght of a paragraph
    var paragraphs = config.paragraphs;
    if (!paragraphs) paragraphs = defaultConfig.paragraphs;
    url = addToUrl(url, paragraphs);
    return url;
}

function addToUrl(url, addition) {
    url += '/' + addition;
    return url;
}

function getRandomInt(min, max) {
    return Math.abs(Math.floor(Math.random() * (max - min + 1)) + min);
}

/**
 * Generates number of random geolocation points given a center and a radius.
 * Reference URL: http://goo.gl/KWcPE.
 * @param  {Object} center A JS object with lat and lng attributes.
 * @param  {number} radius Radius in meters.
 * @return {Object} The generated random points as JS object with lat and lng attributes.
 */
function generateRandomPoint(center, radius) {
    var x0 = parseFloat(center.lng);
    var y0 = parseFloat(center.lat);
    // Convert Radius from meters to degrees.
    var rd = radius / 111300;

    var u = Math.random();
    var v = Math.random();

    var w = rd * Math.sqrt(u);
    var t = 2 * Math.PI * v;
    var x = w * Math.cos(t);
    var y = w * Math.sin(t);

    var xp = x / Math.cos(y0);

    // Resulting point.
    return {
        'lat': y + y0,
        'lng': xp + x0
    };
}


exports.post = handleReq;
exports.get = handleReq;

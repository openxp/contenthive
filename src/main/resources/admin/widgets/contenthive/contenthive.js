var libs = {
    content : require('/lib/xp/content'),
    portal : require('/lib/xp/portal'),
    thymeleaf : require('/lib/thymeleaf'),
    util:require('/lib/util'),
    contentTypeSupplier: __.newBean("openxp.app.contenthive.ContentTypeSupplier")
};

function getContent(contentKey){
    if (contentKey){
        return libs.content.get({key:contentKey});
    }else{
        return libs.portal.getContent();
    }
}

function handleGet(req) {
    var view = resolve('contenthive.html');

    var customConfig = {};
    var contentId = req.params.contentId;
    if (!contentId && libs.portal.getContent()) {
            contentId = libs.portal.getContent()._id;
    }
    if (!contentId) {
        return {
            contentType: 'text/html',
            body: '<widget class="error">No content selected</widget>'
        };
    }

    var content = getContent(contentId);

    var formInputs = libs.contentTypeSupplier.getContenttypeFormInputs(content.type);
    formInputs.forEach(function(formInput){
        if (!formInput){
            return;
        }
        var name = formInput.getName();
        var path = formInput.getPath();
        var value = "";


        if (path.contains(".")){
            var pathParts = path.split(".");
            if (!content.data[pathParts[0]]){//check if item-set is not null
                return;
            }
            value = content.data[pathParts[0]][pathParts[1]];
        }else{
            value = content.data[path];
        }
        if (value){
            formInput.setValue(value);
        }

        if (/^([a-z0-9]{8,}-[a-z0-9]{4,}-[a-z0-9]{4,}-[a-z0-9]{4,}-[a-z0-9]{12,})$/.test(value)){
            var relatedContent = libs.content.get({key:value});
            if (!relatedContent){
                return;
            }
            customConfig[name] = {
                parentPath: relatedContent._path.split('/'+relatedContent._name)[0],
                path: relatedContent._path,
                name: relatedContent._name,
                min: formInput.getMin(),
                max: formInput.getMax()
            };
        }
    });

    var serviceUrl = libs.portal.serviceUrl({service:'newbees'});

    var draft = libs.content.get({
        key: content._id
    });
    var siteConfig = libs.content.getSiteConfig({key:content._id, applicationKey: app.name});

    var model = {
        serviceUrl: serviceUrl,
        parentPath: content._path,
        id: draft._id,
        type: draft.type,
        content: draft,
        customConfig: customConfig,
        appnamespace: app.name.replace(/./g, "_"),
        formInputs: formInputs,
        apiKey: (siteConfig  ? siteConfig.pixabayApikey : ''),
        preSelectInclude: (siteConfig ? siteConfig.preSelectInclude : false)
    };

    return {
        contentType: 'text/html',
        body: libs.thymeleaf.render(view, model)
    };
}


exports.get = handleGet;

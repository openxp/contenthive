var xpContentHiveWidget = (function(){
    console.log("xpContentHiveWidget function");

    var config = {
        widgetId: '#openxp_app_contenthive'
    };

    var init = function(){
        config.formElement = document.querySelector(config.widgetId + ' form');
        if (config.formElement){
            config.formElement.addEventListener("submit", xpContentHiveWidgetForm_submit);
        }
    };

    var xpContentHiveWidgetForm_submit = function(e){
        const serialized = serialize(config.formElement);
        e.preventDefault();

        var url = config.formElement.action+"?"+serialized;
        console.log("Url = " + url);

        xpContentHiveWidgetForm_createRequest(url, xpContentHiveWidgetForm_handleResponse)
    };

    var xpContentHiveWidgetForm_createRequest = function(theUrl, callback)
    {
        console.log("create request to " + theUrl);
        var xmlHttp = new XMLHttpRequest();
        xmlHttp.onreadystatechange = function() {
            if (xmlHttp.readyState == 4 && xmlHttp.status == 200)
                callback(xmlHttp.responseText);
        }
        xmlHttp.open("GET", theUrl, true); // true for asynchronous
        xmlHttp.send(null);
    };

    var xpContentHiveWidgetForm_handleResponse = function(responseText){
        console.log("response: " + responseText);
    };

    return {
        init: init
    };
})()

var xpContentHiveWidgetInit = function(){
    xpContentHiveWidget.init()
};

window.setTimeout(xpContentHiveWidgetInit, 3000);

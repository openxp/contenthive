![Scheme](contenthive-logo.png)

# ContentHive

This widget generates random Lorem Ipsum from the following APIs

## Text

http://loripsum.net

## Images

https://pixabay.com/api/docs (requires signup for free api key).

Note: Access to high resolution requires [full api access](https://pixabay.com/en/service/contact/?full_api_access)

## Releases and Compatibility

See [info about releases on Enonic Market](https://market.enonic.com/vendors/rune-forberg/openxp.app.contenthive)

## Install and configure

1. Add app to your site
2. Add Pixabay API key to app config if you want to generate images
3. Add proxy server:port in app config if needed
4. preSelectInclude = true in app config will check all inputs by default

## Generating images with ContentHive

1. Select a folder
2. Click the second icon in the top right corner and select 'ContentHive' widget in the dropdown
3. Optionally type a query, like 'fish' or select a category to get certain images
4. Editors choice are pretty images, but there are not an unlimited amount of them.
5. See https://pixabay.com/api/docs/ for meaning of available search options
6. Click 'Create' to start generating images in choosen folder

## Generating other content with ContentHive

The purpose of generating random content is to see if your template takes into account different variations of
text / html and related content. Also to save time by not creating stuff manually. When selecting format for
Text and Html areas a number of checkboxes can be ticked (add links, add headers etc.). If 6 boxes are ticked,
then for each created content a random number between 0 and 6 will be generated, if the number is 2, two of the
selected options will be selected for this content. The same way with length of text, a random length will be
selected, you only decide the max number of paragraphs. F.eks. for a preface 'short' and max 2 can be appropriate,
but for body text you might want up to 10 very long paragraphs.

1. Select a content of the type you want to generate. The selected content serves as a dummy/template for the created content
2. If contenttype contains relatedcontent, select a value for each relatedcontent input you wish to be included in generated content
3. Make sure 'include' is checked for each field you want to generate content for (can be pre-selected in app config)
4. For each relatedcontent select 'Use only ...' OR select min/max/folder. Min = 1 and Max = 1 will always choose one relatecontent from selected folder
5. Choose result style for TextArea and HtmlArea, see http://loripsum.net for text-format examples.


    Idea for widget by Rune Forberg and Stepan Shklianko at Enonic Kickoff at Sommarøy, Norway.
